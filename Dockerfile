# centos-systemd-mini

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=0.4
ARG CENTOS_VERSION=7


FROM ${DOCKER_REGISTRY_URL}centos:${CUSTOM_VERSION}-${CENTOS_VERSION} AS centos-systemd-mini

WORKDIR /

RUN \
	yum install -y \
		systemd \
		systemd-sysv \
	; \
	yum clean all ; \
	rpmdb --rebuilddb

RUN \
	systemctl disable \
		getty@tty1.service \
		motd-news.timer \
		ondemand.service \
		remote-fs.target \
		systemd-resolved.service \
		systemd-timesyncd.service \
	; \
	systemctl mask \
		dbus.service \
		dbus.socket \
		getty.target \
		systemd-ask-password-wall.path \
		systemd-initctl.socket \
		systemd-logind.service \
		systemd-remount-fs.service \
		systemd-update-utmp-runlevel.service \
		systemd-user-sessions.service \
	; \
	systemctl set-default multi-user.target

RUN \
	sed -i \
		-e 's|^.\(Storage\)=.*|\1=volatile|' \
		-e 's|^.\(RuntimeMaxUse\)=.*|\1=100M|' \
		-e 's|^.\(ForwardToConsole\)=.*|\1=yes|' \
		/etc/systemd/journald.conf

STOPSIGNAL SIGRTMIN+3

VOLUME /run /run/lock /tmp /sys/fs/cgroup

CMD exec /sbin/init
